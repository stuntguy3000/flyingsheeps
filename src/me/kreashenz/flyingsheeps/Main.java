package me.kreashenz.flyingsheeps;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

public class Main extends JavaPlugin implements Listener {

	protected static int TaskID = 0;

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this,this);
	}

	@EventHandler
	public void onMakeSheepFly(PlayerInteractEntityEvent e){
		if (e.getRightClicked() instanceof Sheep)
		{
			if (e.getPlayer().getItemInHand().getType() == Material.GOLD_INGOT)
			{
				if (e.getPlayer().hasPermission("WhenSheepCanFly.use"))
				{
					final Sheep sheep = (Sheep) e.getRightClicked();

					final BukkitTask Task = new BukkitRunnable() {
						public void run() {

							Bukkit.getServer()
							
							double limit = sheep.getWorld().getMaxHeight();

							if (sheep.getLocation().getY() < limit)
							{
								sheep.teleport(new Location(sheep.getWorld(), 
										sheep.getLocation().getX(), 
										sheep.getLocation().getY() + 4, 
										sheep.getLocation().getZ()));
								sheep.getLocation().getWorld().playEffect(sheep.getLocation(), Effect.SMOKE, 4,4);
								sheep.getLocation().getWorld().playEffect(sheep.getLocation(), Effect.MOBSPAWNER_FLAMES, 4,4);
							} else {
								sheep.setHealth(0);
								sheep.getLocation().getWorld().playEffect(sheep.getLocation(), Effect.SMOKE,4,4);
								sheep.getLocation().getWorld().playEffect(sheep.getLocation(), Effect.MOBSPAWNER_FLAMES, 4,4);
								sheep.getWorld().createExplosion(sheep.getLocation(), 4F);
								killTask();
							}

						}
					}.runTaskTimer(this, 5, 5);
					
					TaskID = Task.getTaskId();
				}
			}
		}
	}
	protected void killTask() {
		Bukkit.getScheduler().cancelTask(TaskID);
	}
}
